import { NegociacaoController } from './controllers/NegociacaoController';
const controller = new NegociacaoController();

// document
//   .querySelector('#form')
//   // controller.adiciona
//   // o this de adiciona não vai apontar pro negociação
//   // bind(controller), 
//   .addEventListener('submit', controller.adiciona.bind(controller) );


$('.form').submit(controller.adiciona.bind(controller));